# No longer in development

# TanukiBot

> Slack bot for GitLab teams

___

# You are welcome to contribute to TanukiBot!

___

## Summary

This Slack bot helps GitLab teams that use Slack to communicate.

Initially TanukiBot will help by providing rich attachments that provide context on and links to issues and merge requests. There's a lot I want to do in the future, a few are in the 'Todo' list below. Community suggestions, issues and merge requests will be welcomed with a big smile.

![Screenshot 1](docs/images/screenshot-1.png)
![Screenshot 2](docs/images/screenshot-2.png)


## Installation

Install TanukiBot:

```shell
git clone git@gitlab.com:lbennett/TanukiBot.git TanukiBot
cd TanukiBot
npm i
```

Set configuration:
```shell
cp -r src/config.example.yml src/config.yml
open config.yml # Edit the configuration to match your needs
```

## Running

Development:

```shell
npm start
```

[Devtools](https://github.com/Jam3/devtool) enabled development w/ autoreload:

```shell
npm run debug
```

Test TanukiBot:
**No tests specified**

___

## Todo

_This list is in no particular order, only numbered for referencing_
1. - [ ] Launch alpha version
2. - [ ] Define a human-readable syntax for users to query GitLab through TanukiBot. For example, `@TanukiBot: What is the status of the most recent master build?`
3. - [ ] Add script to allow easy configuration. Maybe it adds a 'TanukiBot' user to an instances db and returns its private token)
4. - [ ] Add channel-level permissions by allowing multiple private tokens to be set, linked and locked to specific channels. Maybe this can use the script from todo 3 in recursion to generate bulk 'TanukiBot' users and return bulk private tokens
5. - [ ] Integrate with GitLab OAuth so users can connect with TanukiBot individually. For example, `@TanukiBot: How many commits have I made today?`. Maybe this can use the script from todo 3 to configure `application_id` and `application_secret`) ohh and private Todo updates from TanukiBot!
6. - [ ] Add channel-level and user-level default projects
7. - [ ] Allow the creation of issues and merge requests by talking to TanukiBot privately. Maybe this can integrate with a projects default issue/MR descriptions by allowing TanukiBot to engage in a set of asynchronous questions, each being a different header in the default description. For example, if the default markdown is `## Summary... ## Expectation... ## Solution... ## Screenshots...` then the questions from TanukiBot to be answer in sequence would be `['Summary', 'Expectation', 'Solution', 'Screenshots]`
8. - [ ] Fix reference attachment descriptions to truncate at a specific length and format to include specific data
9. - [ ] Finish implementing multiple gitlab instances and configuration intents
10. - [ ] Build GFM to SFM adapter
11. - [ ] For single reference attachments, see if you can show issue/MR upvote/downvotes as Slack reactions. e.g. 30 MR upthumb awards = 30 Slack upthumb reactions, or even add all the reactions when new awards API is released?
12. - [ ] Create 'docs' intent to enable TanukiBot to recognise when someone is asking a question that may be in the docs and try to return those docs
13. - [ ] Add support for regression threads like [this gitlab-ce 8.9 regressions thread](https://gitlab.com/gitlab-org/gitlab-ce/issues/18343) e.g. `@TanukiBot: add #1234 to the 8.9 regressions` (contributed by @connorshea)

___

### WARNING! `PRIVATE_TOKEN` security

The `PRIVATE_TOKEN` property of each GitLab instance in the `config.yml` is used to **determine the permissions** of TanukiBot's GitLab queries, if you use your personal GitLab accounts private token and somebody _(anybody)_ references a confidential issue in one of your private projects, TanukiBot _may_ leak some **private/confidential information**.

I suggest that you set up a new 'TanukiBot' user on each GitLab instance and set its permissions using GitLab so it can **only** access information that is open to anyone using your teams slack.

I hope to integrate channel-level permissions in the future. (or maybe you can!? :D)
