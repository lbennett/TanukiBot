class Intentions
  ###*
   * Manages the processing of message text to extract user intent
   * @method constructor
   * @param  {Config}    @CONFIG Instance of the Config
   * @return {Undefined}
  ###
  constructor: (@CONFIG) ->
    # Sets intent identifier constants
    @INTENT =
      LINK: 'link'
      CONFIG: 'config'
      NONE: 'none'

    # Defines regex to use for intent indentification
    @REGEX =
      REFERENCE: /(#|!)\d+/g
      DIRECT: new RegExp("(@#{@userID})", 'gi') if @CONFIG.USER_ID
      CONFIG: new RegExp("(#{@INTENT.CONFIG}|set|default)+(.|\s)*(repo|project)+", 'gi')
    return

  ###*
   * Sets the userID for intent regexs involving the userID
   * @method setUserID
   * @param  {String}  @userID TanukiBot user ID in the Slack chat
  ###
  setUserID: (@userID) ->
    @REGEX.DIRECT = new RegExp("(@#{@userID})", 'gi')
    return

  ###*
   * Returns a single intent identifier string to help
   * TanukiBot make simpler decisions
   * @method identifyIntent
   * @param  {String}      text Message retrieved from RTM client
   * @return {String}      Intention category string
  ###
  identifyIntent: (text) ->
    if text
      return @INTENT.LINK if @hasLinkIntent text
      return @INTENT.CONFIG if @hasConfigIntent text
    @INTENT.NONE

  ###*
   * Checks the message string for substrings indicating the intention
   * to recieve a link for a specific reference
   * Eg. "Could some one take a look at !12345? it fixes #54321!"
   * @method hasLinkIntent
   * @param  {String}      text Message recieved from RTM client
   * @return {Boolean}     Set to true if link intent exists, false if not
  ###
  hasLinkIntent: (text) ->
    @REGEX.REFERENCE.test text

  ###*
   * Checks the message string for substrings indicating the intention
   * to configure TanukiBot
   * Eg. "@tanukibot can you set the default repo to gitlab-org/gitlab-ce?"
   * @method hasConfigIntent
   * @param  {String}        text Message recieved from RTM client
   * @return {Boolean}       Set to true if config intent exists, false if not
  ###
  hasConfigIntent: (text) ->
    (@targetIsTanukiBot(text) && @REGEX.CONFIG.test text)

  ###*
   * Checks the message string for substrings indicating that
   * the message is targetted at TanukiBot
   * Eg. "@tanukibot got any jokes?"
   * @method targetIsTanukiBot
   * @param  {String}          text Message recieved from RTM client
   * @return {Boolean}          Set to true if TanukiBot is target, false if not
  ###
  targetIsTanukiBot: (text) ->
    @setUserID @CONFIG.USER_ID if !@REGEX.DIRECT && @CONFIG.USER_ID
    (@REGEX.DIRECT && @REGEX.DIRECT.test text)

module.exports = Intentions
