SlackClient = require '@slack/client'
WebClient = SlackClient.WebClient

gitlabClient = require 'gitlab'

class API
  ###*
   * Handles all HTTP API implementations
   * @method constructor
   * @param  {Config}    @CONFIG Instance of the Config
   * @return {Undefined}
  ###
  constructor: (@CONFIG) ->
    @webClient = new WebClient @CONFIG.SLACK.API_TOKEN,
      logLevel: if @CONFIG.DEBUG then 'debug' else 'error'
      autoReconnect: @CONFIG.SLACK.AUTO_RECONNECT
      userAgent: @CONFIG.SLACK.USER_AGENT

    @gitlabClient = gitlabClient
      url: @CONFIG.CURRENT.GITLAB_INSTANCE.URL
      token: @CONFIG.CURRENT.GITLAB_INSTANCE.PRIVATE_TOKEN
    return

  ###*
   * Finds a specific GitLab reference entity e.g. Issue or MR
   * @method findReference
   * @param  {String}      projectID Project to search for reference
   * @param  {Object}      query     Query built by a Reference instance
   * @param  {Function}    cb        Callback
   * @return {Undefined}
  ###
  findReference: (projectID, query, cb) ->
    @gitlabClient.projects[query.tbType].list projectID, query.tbQuery, cb
    return

  ###*
   * Sends a message to a specific channel via the Slack web API
   * @method speak
   * @param  {String}   channel Slack channel ID to send a message to
   * @param  {String}   text Main string of the Slack message
   * @param  {Object}   options Additional options for the Slack message created by a Reference instance
   * @param  {Function} cb      Callback
   * @return {Undefined}
  ###
  speak: (channel, text, options, cb) ->
    @webClient.chat.postMessage channel, text, options, cb
    return

module.exports = API
