path = require 'path'
moment = require 'moment'

class References
  ###*
   * Handles all actions involving references to GitLab entities like issues and MRs
   * @method constructor
   * @param  {API}    @API    Instance of the API
   * @param  {Config}    @CONFIG Instance of the Config
   * @return {Undefined}
  ###
  constructor: (@API, @CONFIG) ->
    @REFERENCE_TYPES =
      MERGE_REQUESTS:
        NAME: 'merge_requests'
        IDENTIFIER: '!'
      ISSUES:
        NAME: 'issues'
        IDENTIFIER: '#'

    @STATE_TYPES =
      OPENED:
        COLOR: '#38ae67'
      CLOSED:
        COLOR: '#d22852'
      MERGED:
        COLOR: '#2d9fd8'
    return

  ###*
   * Fetches data on references found then builds and sends a reply
   * @method linkToReferences
   * @param  {String}         channel      Slack channel ID to reply to
   * @param  {Array}         referenceIDs Collection of reference IDs to search for
   * @return {Undefined}
  ###
  linkToReferences: (channel, referenceIDs) ->
    @findReferences referenceIDs, (populatedReferences) =>
      @API.speak channel, null, @buildReferenceReply(populatedReferences), (err, info) ->
        #TODO: What to do?
    return

  ###*
   * Loops a collection of reference IDs and populates an array of fetched references to return
   * @method findReferences
   * @param  {Array}       referenceIDs        Collection of reference IDs to search for
   * @param  {Function}     cb                  Callback
   * @param  {Array}       populatedReferences Collection of fetched references
   * @param  {Number}       i                   Recursive iterator counter
   * @return {Undefined}
  ###
  findReferences: (referenceIDs, cb, populatedReferences, i) ->
    i = i || 0
    populatedReferences = populatedReferences || []
    referenceQuery = @buildReferenceQuery(referenceIDs[i])
    @API.findReference @CONFIG.CURRENT.GITLAB_INSTANCE.DEFAULT_PROJECT, referenceQuery, (referenceData) =>
      populatedReference = referenceData[0]
      if populatedReference
        populatedReference = @attachTanukiBotProperties referenceQuery, populatedReference
        populatedReferences.push populatedReference

      if i < (referenceIDs.length - 1)
        i++
        @findReferences referenceIDs, cb, populatedReferences, i
      else
        cb populatedReferences if cb
    return

  ###*
   * Attached custom properties to fetch references used later by TanukiBot
   * @method attachTanukiBotProperties
   * @param  {Object}                  referenceQuery A query built with '@buildReferenceQuery'
   * @param  {String}                  referenceQuery.tbID A full reference to the reference including identifier
   * @param  {Object}                  referenceQuery.tbQuery Specifies the query params for the request
   * @param  {String}                  referenceQuery.tbType Defines the type of reference
   * @param  {Object}                  reference      A fetched reference
   * @return {Object}                  The fetched reference + additional properties
  ###
  attachTanukiBotProperties: (referenceQuery, reference) ->
    reference.tbType = referenceQuery.type
    reference.tbID = referenceQuery.tbID
    reference.tbStateColor = @STATE_TYPES[reference.state.toUpperCase()].COLOR
    reference.tbLink = path.join @CONFIG.CURRENT.GITLAB_INSTANCE.URL, @CONFIG.CURRENT.GITLAB_INSTANCE.DEFAULT_PROJECT,
    @REFERENCE_TYPES[reference.tbType.toUpperCase()].NAME, reference.iid.toString()
    reference

  ###*
   * Builds a query to be converted to params for an API request
   * @method buildReferenceQuery
   * @param  {String}            referenceID The full reference including identifier
   * @return {Object}            The query for the request
  ###
  buildReferenceQuery: (referenceID) ->
    referenceQuery = {}
    referenceQuery.tbID = referenceID
    referenceQuery.tbQuery = iid: referenceID.substring(1)
    referenceQuery.tbType = switch referenceID.charAt(0)
      when @REFERENCE_TYPES.ISSUES.IDENTIFIER
        @REFERENCE_TYPES.ISSUES.NAME
      when @REFERENCE_TYPES.MERGE_REQUESTS.IDENTIFIER
        @REFERENCE_TYPES.MERGE_REQUESTS.NAME
    referenceQuery

  ###*
   * Builds the Slack web API message to replies including references
   * @method buildReferenceReply
   * @param  {Array}            references Collection of populated references + custom properties
   * @return {Object}            A Slack message for the web API
  ###
  buildReferenceReply: (references) ->
    replyMessage =
      as_user: true
      attachments: []
    for reference in references
      replyMessage.attachments.push
        fallback: "#{reference.tbID} | #{reference.title} | [#{reference.state}]",
        color: reference.tbStateColor,
        title: "#{reference.tbID} #{reference.title}"
        title_link: reference.tbLink
        text: "#{reference.description}\n"
        footer: reference.author.name
        footer_icon: reference.author.avatar_url
        ts: moment(reference.created_at).unix()
    replyMessage

module.exports = References
