path = require 'path'
fs = require 'fs'
yaml = require 'js-yaml'

class Config
  ###*
   * Parses, validates and manages the users configuration
   * @method constructor
   * @return {Undefined}
  ###
  constructor: ->
    @__CONFIG = yaml.safeLoad fs.readFileSync path.join(__dirname, '../config.yml'), 'utf8'
    @DEBUG = if 'DEBUG' in process.env then process.env.DEBUG else @__CONFIG.DEBUG || false
    @SLACK = @__CONFIG.SLACK
    @CURRENT = {}

    # Validates the instance entries and sets the default instance
    @validateGitLabInstances @__CONFIG.GITLAB.INSTANCES

    throw new Error('no Slack API token set') if !@__CONFIG.SLACK.API_TOKEN
    return

  ###*
   * Validates the array of GitLab instances data
   * @method validateGitLabInstances
   * @param  {Array}                instances Collection of user-defined GitLab instance data
   * @return {Undefined}
  ###
  validateGitLabInstances: (instances) ->
    for instance in instances
      # Set current instance using default
      @CURRENT.GITLAB_INSTANCE = instance if instance.DEFAULT
      # Validate instances
      throw new Error('no identifier set for GitLab instance') if !instance.IDENTIFIER
      throw new Error('no url set for GitLab instance') if !instance.URL
      throw new Error('no default project set for GitLab instance') if !instance.DEFAULT_PROJECT
      throw new Error('no private token set for GitLab instance') if !instance.PRIVATE_TOKEN

    throw new Error('no default GitLab instance set') if !@CURRENT.GITLAB_INSTANCE
    return

module.exports = Config
