path = require 'path'

SlackClient = require '@slack/client'
RtmClient = SlackClient.RtmClient
RTM_CLIENT_EVENTS = SlackClient.CLIENT_EVENTS.RTM
RTM_EVENTS = SlackClient.RTM_EVENTS

API = require path.join __dirname, 'API'
Config = require path.join __dirname, 'Config'
Intentions = require path.join __dirname, 'Intentions'
References = require path.join __dirname, 'References'

class TanukiBot
  ###*
   * Performs the interactive tasks of TanukiBot like listening to messages
   * and processing interactions.
   * @method constructor
   * @return {Undefined}
  ###
  constructor: () ->
    @CONFIG = new Config()

    @rtmClient = new RtmClient @CONFIG.SLACK.API_TOKEN,
      logLevel: if @CONFIG.DEBUG then 'debug' else 'error'
      autoReconnect: @CONFIG.SLACK.AUTO_RECONNECT
    @initializeRTM()

    @intentions = new Intentions @CONFIG

    @API = new API @CONFIG

    @references = new References @API, @CONFIG
    return

  ###*
   * Starts the RTM client, binds the auth event and wait for a connection
   * to begin binding further events
   * @method initializeRTM
   * @return {Undefined}
  ###
  initializeRTM: ->
    @rtmClient.on RTM_CLIENT_EVENTS.AUTHENTICATED, @onAuthenticated, this
    # Wait for RTM connection before binding client events
    @rtmClient.on RTM_CLIENT_EVENTS.RTM_CONNECTION_OPENED, @bindRTM, this
    @rtmClient.start()
    return

  ###*
   * Binds instance methods to required RTM client events
   * @method bindRTM
   * @return {Undefined}
  ###
  bindRTM: ->
    @rtmClient.on RTM_EVENTS.MESSAGE, @onMessage, this
    return

  ###*
   * Captures the TanukiBot user ID on RTM client authentication success
   * @method onAuthenticated
   * @param  {Object}        authenticationData RTM provided authentication data
   * @return {Undefined}
  ###
  onAuthenticated: (authenticationData) ->
    @CONFIG.USER_ID = authenticationData.self.id
    return

  ###*
   * Processes a recieved message by extracting the intent
   * using an Intentions instance and then processes accordingly, if required
   * @method onMessage
   * @param  {Object}  message Slack message recieved via RTM
   * @return {Undefined}
  ###
  onMessage: (message) ->
    return if !@intentions
    # Select an intention-dependent method
    message.intent = @intentions.identifyIntent(message.text)
    switch message.intent
      when @intentions.INTENT.LINK
        @references.linkToReferences message.channel,
        message.text.match @intentions.REGEX.REFERENCE
      when @intentions.INTENT.CONFIG then @configureSettings message
    return

  ###*
   * Sets TanukiBot configuration values either for a user or team
   * @method configureSettings
   * @param  {Object}          message Slack message recieved via RTM
   * @return {Undefined}
  ###
  configureSettings: (message) ->
    console.log 'send config message'
    @API.speak message.intent, message.channel, ->
      console.log 'sent configure'
    return

module.exports = TanukiBot
