gulp = require 'gulp'
coffee = require 'gulp-coffee'
coffeelint = require 'gulp-coffeelint'
sourcemaps = require 'gulp-sourcemaps'
del = require 'del'

gulp.task 'lint:coffee', ->
  gulp.src './src/**/*.coffee'
        .pipe coffeelint()
        .pipe coffeelint.reporter()

gulp.task 'build:coffee', ->
  gulp.src './src/**/*.coffee'
    .pipe sourcemaps.init()
    .pipe coffee()
    .pipe sourcemaps.write()
    .pipe gulp.dest './dist/'

gulp.task 'build:yaml', ->
  gulp.src './src/**/*.yml'
    .pipe gulp.dest './dist/'

gulp.task 'watch:coffee', ->
  gulp.watch './src/**/*.coffee', ['lint:coffee', 'build:coffee']

gulp.task 'watch:yaml', ->
  gulp.watch './src/**/*.yml', ['build:yaml']

gulp.task 'clean', ->
  del ['./dist']

gulp.task 'lint', ['lint:coffee']

gulp.task 'build', ['build:yaml', 'build:coffee']

gulp.task 'watch', ['watch:yaml', 'watch:coffee']

gulp.task 'default', ['lint', 'build']

gulp.task 'dev', ['default', 'watch']
